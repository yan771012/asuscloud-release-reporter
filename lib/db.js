var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ImageInfo = new Schema({
    fileName: {type:String, default:""},
    extension: {type:String, default:""},
    comment: {type:String, default:""}
});

var ReportItem = new Schema({
    title: {type: String, default: ""},
    result: {type: Boolean, default: true},
    testDate: {type: Date, default: Date.now},
    testStep: {type: String, default: ""},
    expectedResult: {type: String, default: ""},
    realityResultText: {type: String, default: ""},
    realityResultImages:[ImageInfo]
});

var Reporter = new Schema( {
    userName: {type: String, default: ""},
    reporterTitle: {type: String, default: ""},
    createDate: {type: Date, default: Date.now},
    reportItems: [ReportItem]
});

mongoose.model('Reporter', Reporter);
mongoose.model('ReportItem', ReportItem);
mongoose.model('ImageInfo', ImageInfo);
mongoose.connect('mongodb://localhost/Reporter');