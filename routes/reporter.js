var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Reporter = mongoose.model('Reporter');
var ReportItem = mongoose.model('ReportItem');
var ImageInfo = mongoose.model('ImageInfo');
var fs = require('fs');
var extend = require('extend');

router.put('/report/updateReportItems/:reporterId', function (req, res, next) {
    var itemArray = JSON.parse(req.body.itemArray);
    Reporter.find({'_id': req.params.reporterId}, function (err, reporters) {
        var reporter = reporters[0];
        var newReportItems = [];

        for (var i = 0; i < itemArray.length; i++) {
            var item = itemArray[i];
            var reportItem = getReportItem(reporter, item._id);
            extend(reportItem, item);
            newReportItems.push(reportItem);
        }
        reporter.reportItems = newReportItems;
        reporter.save(function (err) {
            if (err) throw err;
            res.sendStatus(200);
        });
    });
});

router.post('/report/addReportItem', function (req, res, next) {
    Reporter.find({'_id': req.body.reporterId}, function (err, reporters) {
        var reporter = reporters[0];
        var newItem = new ReportItem;
        newItem.title = req.body.itemTitle;
        reporter.reportItems.push(newItem);
        reporter.save(function (err) {
            if (err) throw err;
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(newItem));
        });
    });
});

router.get('/findReport', function (req, res, next) {
    var userName = decodeURIComponent(req.query.userName);
    Reporter.find({'userName': userName}, function (err, reporters) {
        var result = [];
        for (var i = 0; i < reporters.length; i++) {
            var report = reporters[i];
            result.push({
                title: report.reporterTitle,
                id: report._id,
                createDate: report.createDate
            });
        }
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(result));
    });
});

router.get('/:reporterId', function (req, res, next) {
    Reporter.find({'_id': req.params.reporterId}, function (err, reporters) {
        var reporter = reporters[0];
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(reporter));
    });
});

router.get('/report/getItems', function (req, res, next) {
    Reporter.find({'_id': req.query.reporterId}, function (err, reporters) {
        var reporter = reporters[0];
        var items = [];
        for (var idx = 0; idx < reporter.reportItems.length; idx++) {
            var item = reporter.reportItems[idx];
            items.push({title: item.title, id: item._id});
        }
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(items));
    });
});

router.put('/report/updateReportItemResult', function (req, res, next) {
    Reporter.find({'reportItems._id': req.body.itemId}, function (err, reporters) {
        var reporter = reporters[0];
        var reportItem = getReportItem(reporter, req.body.itemId);

        if (reportItem !== undefined) {
            reportItem.result = req.body.state;

            reporter.save(function (err) {
                if (err) throw err;
                res.sendStatus(200);
            });
            return;
        }
        res.sendStatus(404);
    });
});

router.delete('/report/removeImage', function (req, res, next) {
    Reporter.find({'reportItems._id': req.body.itemId}, function (err, reporters) {
        var reporter = reporters[0];
        var reportItem = getReportItem(reporter, req.body.itemId);
        var imageId = req.body.imageId;
        var targetImg;

        if (reportItem !== undefined) {
            for (var i = 0; i < reportItem.realityResultImages.length; i++) {
                var realityResultImage = reportItem.realityResultImages[i];
                if (realityResultImage.id == imageId) {
                    targetImg = realityResultImage;
                    break;
                }
            }
            var idx = reportItem.realityResultImages.indexOf(targetImg);
            if (targetImg !== undefined && idx > -1) {
                reportItem.realityResultImages.splice(idx, 1);
            }

            reporter.save(function (err) {
                if (err) throw err;

                var removeFile = './public/upload/' + targetImg.id + '.' + targetImg.extension;
                fs.unlink(removeFile, function() {
                    if (err) throw err;
                    console.log('successfully deleted:' + removeFile);
                });
                res.sendStatus(200);
            });
            return;
        }
        res.sendStatus(404);
    });
});

router.post('/report/upload/:itemId', function (req, res, next) {
    Reporter.find({'reportItems._id': req.params.itemId}, function (err, reporters) {
        var reporter = reporters[0];
        var reportItem = getReportItem(reporter, req.params.itemId);
        var addImg = [];

        if (reportItem !== undefined) {
            for (var key in req.files) {
                var fileInfo = req.files[key];
                var imageInfo = new ImageInfo({
                    fileName: fileInfo.originalname,
                    extension: fileInfo.extension
                });
                var tmpPath = fileInfo.path;
                var targetFolderPath = './public/upload/';
                var targetFileName = imageInfo.id + '.' + fileInfo.extension;
				var source = fs.createReadStream(tmpPath);
				var dest = fs.createWriteStream(targetFolderPath + '/' + targetFileName);
				source.pipe(dest);
				source.on('error', function(err) { throw err; });
                reportItem.realityResultImages.push(imageInfo);
                addImg.push(imageInfo);
            }
            reporter.save(function (err) {
                if (err) throw err;
                res.setHeader('Content-Type', 'application/json');
                var returnResult = {
                    _id: reportItem.id,
                    realityResultImages: addImg
                };
                res.end(JSON.stringify(returnResult));
            });
        }
    });

});

router.get('/report/:reporterId', function (req, res, next) {
    res.render('reportList');
});

router.post('/createReport', function (req, res, next) {
    var listContent = req.body.reportList.split(/\n/);
    var listArr = [];
    for (var i = 0; i < listContent.length; i++) {
        // only push this line if it contains a non whitespace character.
        if (/\S/.test(listContent[i])) {
            listArr.push(listContent[i].trim());
        }
    }
    var reportItems = [];
    for (i = 0; i < listArr.length; i++) {
        reportItems.push(new ReportItem({
            title: listArr[i]
        }));
    }
    new Reporter({
        userName: req.body.userName,
        reporterTitle: req.body.reporterTitle,
        reportItems: reportItems
    }).save(function (err, reporter) {
            if (err) throw err;
            res.redirect('/reporter/report/' + reporter.id);
    });
});

function getReportItem (reporter, itemId) {
    var target;
    for (var idx = 0; idx < reporter.reportItems.length; idx++) {
        var item = reporter.reportItems[idx];
        if (item.id == itemId) {
            target = item;
            break;
        }
    }
    return target;
}

module.exports = router;
