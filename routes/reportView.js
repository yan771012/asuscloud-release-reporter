var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Reporter = mongoose.model('Reporter');
var ReportItem = mongoose.model('ReportItem');
var ImageInfo = mongoose.model('ImageInfo');

/* GET users listing. */
router.get('/:reporterId', function (req, res, next) {
    Reporter.find({'_id': req.params.reporterId}, function (err, reporters) {
        if (reporters != undefined && reporters.length == 1) {
            var reporter = reporters[0];
            res.render('reportView', {reporter: reporter});
        } else {
            res.send('找不到報告！');
        }
    });
});

module.exports = router;
